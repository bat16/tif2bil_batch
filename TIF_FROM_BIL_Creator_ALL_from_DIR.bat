@ECHO OFF
chcp 850
setlocal enabledelayedexpansion
ECHO TIF Creator from BIL files in folder
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 3.8
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET WORK=%cd%
SET COORDINATE=3006

REM COUNTER FILES
dir /b /s *.bil 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1


FOR /F %%i IN ('dir /b/s "*.bil" ') DO (
    ECHO.
	ECHO Processing  %%~ni    !Counter! / %count% FILES 
	
	ECHO CONVERTING BIL TO TIF 
	gdalwarp -co "TFW=YES" -co PHOTOMETRIC=RGB -of GTiff -co COMPRESS=LZW -wo NUM_THREADS=ALL_CPUS -s_srs EPSG:%COORDINATE% -t_srs EPSG:%COORDINATE% -srcnodata "0 0 0 " -dstnodata "0 0 0 " %%i %%~pi%%~ni.tif
	set /A Counter+=1
	DEL /Q "%WORK%\tile.*"
	DEL /Q "%WORK%\cutter.*"	
)
ECHO.
ECHO Done
PAUSE