@ECHO OFF
setlocal enabledelayedexpansion
ECHO TIF Creator from BIL files in folder
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal

REM Path to working dir
SET WORK=%cd%
if not exist "%WORK%\TFW" mkdir "%WORK%\TIF"

REM COUNTER FILES
dir /b *.bil 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.bil"') DO (
    ECHO.
	ECHO Processing  %%i    !Counter! / %count% FILES
	gdal_translate -of GTiff -ot Byte -co "TFW=YES" -co "COMPRESS=LZW" -a_nodata "0 0 0" -a_srs EPSG:3006 %%i "TIF\%%~ni.tif"
	set /A Counter+=1			
)
ECHO.
ECHO Done
PAUSE