@ECHO OFF
chcp 850
setlocal enabledelayedexpansion
ECHO TIF Creator from BIL files in folder
ECHO Author: Szczepkowski Marek
ECHO Date: 25-06-2019
ECHO Version: 1.0
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 3.8
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET WORK=%cd%
SET COORDINATE=3006

REM COUNTER FILES
dir /b /s *.bil 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1


FOR /F %%i IN ('dir /b/s "*.bil" ') DO (
    ECHO.
	ECHO Processing  %%~ni    !Counter! / %count% FILES 
	
	REM create index of tiled tif
	ECHO CREATE TILE
	gdaltindex -tileindex location -f "ESRI Shapefile" "%WORK%\tile.shp" %%i

	REM intersection tiled with buffer
	ECHO CREATE CUTTER BUFFER
	ogr2ogr -clipsrc "%WORK%\tile.shp" "%WORK%\cutter.shp" "%WORK%\*powerline.shp"

	ECHO CROPPING 
	gdalwarp -co "TFW=YES" -co PHOTOMETRIC=RGB -of GTiff -wo NUM_THREADS=ALL_CPUS -s_srs EPSG:%COORDINATE% -t_srs EPSG:%COORDINATE% -srcnodata "0 0 0 " -dstnodata "0 0 0 " -cutline "%WORK%\cutter.shp" %%i %%~pi%%~ni.tif
	set /A Counter+=1
	DEL /Q "%WORK%\tile.*"
	DEL /Q "%WORK%\cutter.*"	
)
ECHO.
ECHO Done
PAUSE